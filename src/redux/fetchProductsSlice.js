import { createSlice } from "@reduxjs/toolkit";

const fetchProductsReducer = createSlice({
    name: "fetchProducts",
    initialState: {
        productList: [],
    },
    reducers: {
        addProducts: (state, {payload}) => {
            state.productList = payload
        }
    }
});

export const { addProducts:addProductsAction } = fetchProductsReducer.actions;

export default fetchProductsReducer.reducer;