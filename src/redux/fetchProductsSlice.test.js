import { expect } from '@jest/globals';
import reducer from './fetchProductsSlice';

describe('Reducer works', () => {
    test('should Reducer test', () => {
        const state = { productList: [] };
        const action = { type: 'fetchProducts/addProducts', payload: [{}, {}]};

        expect(reducer(state, action)).toEqual({ productList: [{}, {}] });
    });

});
