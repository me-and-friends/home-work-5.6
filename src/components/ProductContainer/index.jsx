import styles from './ProductContainer.module.scss';
import ProductCard from '../ProductCard';
import PropTypes from 'prop-types';

const ProductContainer = ({ products = [], addToFavorite=() => {}, isFavorite = () => {}, modalActivate, setCurrentProduct}) => {
    return (
        <div className={styles.productContainer}>
            {products.map(product => (
                <ProductCard key={product.id} product={product} addToFavorite={addToFavorite} isFavorite={isFavorite} modalActivate={modalActivate} setCurrentProduct={setCurrentProduct}/>
            ))}
        </div>
    );
}

ProductContainer.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string,
            author: PropTypes.string,
            genre: PropTypes.string,
            cover: PropTypes.string.isRequired,
        })
    )
}

export default ProductContainer;
