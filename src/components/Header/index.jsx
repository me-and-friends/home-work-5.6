import styles from './Header.module.scss';
import CartIcon from '../svg/CartIcon';
import StarIcon from '../svg/StarIcon'
import { NavLink } from 'react-router-dom';

const Header = ({ favorite = [], cartItems }) => {
  return (
    <header className={styles.header}>
      <span className={styles.logo}>Rozetka Books</span>

      <nav>
        <ul>
          <li>
            <NavLink end to='/' className={({ isActive }) => isActive && styles.active}>Home</NavLink>
          </li>

          <li>
            <div className={styles.containers}>
              <NavLink end to='/cart' className={({ isActive }) => isActive && styles.active}>
                <CartIcon />
                <span>{cartItems.length}</span>
              </NavLink>

              <NavLink end to='/favorite' className={({ isActive }) => isActive && styles.active}>
                <StarIcon />
                <span>{favorite.length}</span>
              </NavLink>
            </div>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
