import { expect } from '@jest/globals';

import ModalBody from "./index";
import { render, screen, fireEvent } from '@testing-library/react';
import { useState } from 'react';

// modalActivate = () => { };
const curentProduct = {
    title: "Kobzar",
    author: "Taras Shevchenko",
    genre: "Classic",
    cover: "https://i.pinimg.com/564x/30/c6/0b/30c60b710ce78c91daf10c2c464883fa.jpg",
    id: 0
};
addToCart = () => { };

const Wrapper = () => {
    const [modalActivate, setModalActivate] = useState(false);

    return (
        <>
            <button onClick={() => {setModalActivate(true)}}>open</button>
            {modalActivate && <ModalBody modalActivate={modalActivate} curentProduct={curentProduct} addToCart={addToCart} />}
        </>
    )
};

describe('Modal open/close', () => {
    test('should render on open', () => {
        const {getByRole} = render(<Wrapper/>);

        const btn = getByRole("button", {name: /open/i});
        fireEvent.click(btn);

        expect(screen.getByTestId('modal')).toBeInTheDocument();
    });
});