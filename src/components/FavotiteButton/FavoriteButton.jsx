import styles from './FavoriteButton.module.scss';
import star from './svg/star.svg'
import starActive from './svg/starActive.svg'
import { useEffect, useState } from 'react';

const FavoriteButton = ({addToFavorite = () => {}, product = [], isFavorite = () => {}}) => {

    const [isClicked, setIsClicked] = useState(false);

    return (
        <button className={styles.star} onClick={() => {
            // setIsClicked(!isClicked);
            addToFavorite(product, setIsClicked)
        }}>
            <img src={isClicked ? starActive : star} alt="star" />
        </button>
    );
}

export default FavoriteButton;
