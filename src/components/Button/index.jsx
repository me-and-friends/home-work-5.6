import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({ children, onClick}) => {
    

    return (
        <>
            <button className={styles.button} onClick={onClick} data-testid="buttonTestID">{children}</button>
        </>
    );
}

Button.propTypes = {
    children: PropTypes.string.isRequired
}

export default Button;
