import { expect } from '@jest/globals';

import Button from "./index";
import { render, screen, fireEvent} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const callback = jest.fn();

describe('Button snapshot testing', () => {
    test('should Button render', () => {
        const { asFragment } = render(<Button className="button">Add to cart</Button>);

        expect(asFragment()).toMatchSnapshot();
    });

    test('should onClick works', () => {
        const { getByTestId } = render(<Button onClick={callback}>Add to cart</Button>);

        const btn = getByTestId("buttonTestID");

        console.log(btn);

        fireEvent.click(btn);

        expect(callback).toHaveBeenCalled();
    });
});