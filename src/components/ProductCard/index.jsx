import React from 'react';
import styles from './ProductCard.module.scss'
import Button from '../Button'
import ModalBody from '../Modal/ModalBody';
import FavoriteButton from '../FavotiteButton/FavoriteButton';

const ProductCard = ({ product = {}, addToFavorite = () => { }, isFavorite = () => { }, modalActivate, setCurrentProduct }) => {
    const handleClick = () => {
        modalActivate();
        setCurrentProduct(product);
    }

    return (
        <div className={styles.cardBody}>
            <FavoriteButton addToFavorite={addToFavorite} product={product} isFavorite={isFavorite} />
            <img src={product.cover} alt="cover" className={styles.cardImg} />
            <span className={styles.cardTitle}>{product.title}</span>
            <span className={styles.cardAuthor}>{product.author}, {product.genre}</span>
            <Button onClick={ handleClick }>Add to cart</Button>
        </div>
    );
}

export default ProductCard;
