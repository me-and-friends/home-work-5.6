import React from 'react';
import styles from './Favorite.module.scss';
import cross from './cross.svg';

const Favorite = ({ item, addToFavorite }) => {
    return (
        <div className={styles.container}>
            <button className={styles.cross} onClick={() => addToFavorite(item)}><img src={cross} alt='cross' /></button>
            <span className={styles.title}>{item.title}</span>
            <img src={item.cover} alt='img' className={styles.img} />
        </div>
    );
}

export default Favorite;
