import React from 'react';
import styles from './Cart.module.scss';
import cross from './cross.svg';

const Cart = ({ item, removeCart = () => {} }) => {
    return (
        <div className={styles.container}>
            <button className={styles.cross} onClick={() => removeCart(item)}><img src={cross} alt='cross' /></button>
            <span className={styles.title}>{item.title}</span>
            <img src={item.cover} alt='img' className={styles.img} />
        </div>
    );
}

export default Cart;
