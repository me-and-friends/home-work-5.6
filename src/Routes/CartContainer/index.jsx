import React from 'react';
import Cart from '../Cart';
import styles from './CartContainer.module.scss';
import PaymentForm from './Form';

const CartContainer = ({ cartItems = [], removeCart = () => { } }) => {

    return (
        <div className={styles.cartContainer}>
            {/* <button onClick={console.log(cartItems)}>button</button> */}
            {cartItems.map(item => (
                <Cart key={item.id} item={item} removeCart={removeCart} />
            ))}
            <div className={styles.form}>
                <PaymentForm />
            </div>
        </div>
    );
}

export default CartContainer;
