import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import styles from './Form.module.scss';


const validationSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: Yup.number()
        .min(18, 'Too young')
        .max(110, 'Do people live so long?')
        .required('Required'),
    phone: Yup.number()
        .min(2, 'Too short')
        .required('Required'),
    adress: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
});

const PaymentForm = () => {

    return (
        <Formik
            initialValues={{
                name: '',
                lastName: '',
                age: '',
                phone: '',
                adress: '',
            }}

            onSubmit={(values, { resetForm }) => {
                console.log(values);
                console.log(localStorage.getItem("cartItems"));
                localStorage.clear("cartItems");
                alert("Sucess transaction. Have a nice day!");
                resetForm();
            }}

            validationSchema={validationSchema}
        >
            {(form) => {

                return (
                    <Form>
                        <Field
                            type="text"
                            name="name"
                            placeholder="First Name"
                        />
                        <ErrorMessage name='name'>{(err) => <p style={{ color: 'red' }}>{err}</p>}</ErrorMessage>

                        <Field
                            type="text"
                            name="lastName"
                            placeholder="Last Name"
                        />

                        <ErrorMessage name='lastName'>{(err) => <p style={{ color: 'red' }}>{err}</p>}</ErrorMessage>

                        <Field
                            type="number"
                            name="age"
                            placeholder="Your age"
                        />

                        <ErrorMessage name='age'>{(err) => <p style={{ color: 'red' }}>{err}</p>}</ErrorMessage>

                        <Field
                            type="number"
                            name="phone"
                            placeholder="Your phone number"
                        />

                        <ErrorMessage name='phone'>{(err) => <p style={{ color: 'red' }}>{err}</p>}</ErrorMessage>

                        <Field
                            type="text"
                            name="adress"
                            placeholder="Delivery Adress"
                        />

                        <ErrorMessage name='adress'>{(err) => <p style={{ color: 'red' }}>{err}</p>}</ErrorMessage>

                        <button type="submit">Submit</button>
                    </Form>
                )
            }}
        </Formik>
    )
}

export default PaymentForm;
